package com.zmk666.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zmk666.homework.Cat;
import com.zmk666.homework.MyConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=MyConfig.class)
public class TestHomework {
	
	@Autowired
	private Cat cat;
	@Test
	public void test1() throws Exception {
		System.out.println(cat);
	}
}
