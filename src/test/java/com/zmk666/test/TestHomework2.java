package com.zmk666.test;



import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zmk666.homeworktwo.Company;
import com.zmk666.homeworktwo.MyConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=MyConfig.class)
public class TestHomework2 {

	@Autowired
	private Company company;
	
	
	@Test
	public void test1() throws Exception {
		System.out.println(company);
	}
}
