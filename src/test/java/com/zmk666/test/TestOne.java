package com.zmk666.test;


import java.util.Date;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zmk666.haha.MyConfig;
import com.zmk666.haha.Person;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=MyConfig.class)
public class TestOne {

	@Autowired
	private Date date;
	
	@Autowired
	private Person person;
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Test
	public void test1() throws Exception {
		System.out.println(date);
		System.out.println(person);
		String[] names = applicationContext.getBeanDefinitionNames();
		for (String name : names) {
			System.out.printf("name:%s;type:%s;\n",name,applicationContext.getBean(name).getClass());
		}
	}
	
	@Test
	public void test2() throws Exception {
		Map<String, Object> map = applicationContext.getBeansOfType(Object.class);
		System.out.println(map);
	}
	
}
