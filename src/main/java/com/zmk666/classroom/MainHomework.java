package com.zmk666.classroom;

import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.zmk666.mainhomework.MyConfig;
import com.zmk666.mainhomework.Xiaoming;

public class MainHomework {

	public static void main(String[] args) {
		ApplicationContext ac = new AnnotationConfigApplicationContext(MyConfig.class);
		
		//通过getBeansOfType()方法 根据  *类型*  获取该类类型和子类类型的对象
		Map<String, Xiaoming> beansOfType = ac.getBeansOfType(Xiaoming.class);
		System.out.println(beansOfType);
		
		//获取容器中所有对象的名称
		String[] names = ac.getBeanDefinitionNames();
		for (String name : names) {
			System.out.print(name+"  ");
			System.out.println(ac.getBean(name));
		}
		
		Xiaoming bean = (Xiaoming) ac.getBean("xiaoming");
		System.out.println(bean);
		
		Xiaoming bean2 = ac.getBean(Xiaoming.class);
		System.out.println(bean2);
	}

}
