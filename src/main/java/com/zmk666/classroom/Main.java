package com.zmk666.classroom;

import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.zmk666.haha.MyConfig;
import com.zmk666.haha.Person;

public class Main {
	public static void main(String[] args) {
		ApplicationContext ac = new AnnotationConfigApplicationContext(MyConfig.class);
		
		//根据类型获取该类类型和子类类型的对象
		Map<String, Person> person = ac.getBeansOfType(Person.class);
		System.out.println(person);
		
		//获取容器中所有对象的名称
		String[] names = ac.getBeanDefinitionNames();
		for (String name : names) {
			System.out.println(ac.getBean(name));
		}
		
		//通过类型获取
		Person bean = ac.getBean(Person.class);
		System.out.println(bean);
		
		//通过名称获取
		/**
		 * 名称怎么来：
		 * @Component注解注入的，根据类名首字母小写
		 * @Bean注解注入的，就是方法名
		 */
		Object bean2 = ac.getBean("person");
		System.out.println(bean2);
		
		Object bean3 = ac.getBean("date");
		System.out.println(bean3);
		
	}
}
