package com.zmk666.classroom;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.zmk666.homeworktwo.MyConfig;

public class Main2 {

	public static void main(String[] args) {
		ApplicationContext ac = new AnnotationConfigApplicationContext(MyConfig.class);
		Object bean = ac.getBean("company");
		System.out.println(bean);
	}

}
