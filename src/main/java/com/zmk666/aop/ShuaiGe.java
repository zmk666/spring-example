package com.zmk666.aop;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

//@Aspect
@Component
public class ShuaiGe {
	
    //切点        //  返回值       全方法名
	@Pointcut("execution(* com.zmk666.*.*.*(..))")
	public void haha() {}

	//在方法执行之前进行拦截 
	@Before("com.zmk666.aop.ShuaiGe.haha()")
	public void befor() {
		System.out.println("要不我来付吧");
	}
	
	//在方法执行之后拦截
	@After("haha()")
	public void after() {
		System.out.println("-------后置通知-------");
		System.out.println("好！下次我来付！");
	}
	
	//返回值通知
	@AfterReturning("haha()")
	public void retn() {
		System.out.println("-------返回值通知-------");
		System.out.println("有返回值");
	}
	
	//异常通知
	@AfterThrowing("haha()")
	public void except() {
		System.out.println("出错了");
	}
	
}
