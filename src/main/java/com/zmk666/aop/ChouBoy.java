package com.zmk666.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ChouBoy {

	@Around("com.zmk666.aop.ShuaiGe.haha()")
	public Object round(ProceedingJoinPoint pjp) {
		Object[] args = pjp.getArgs();
		try {
			Object r = pjp.proceed(args);
			System.out.println("今天很开心，明天再约好吧！");
			return r;
		} catch (Throwable e) {
			System.out.println("没钱就不是个事，我卖血也把钱付了");
		}
		return null;
	}
}
