package com.zmk666.aop;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

//@Order(1)
@Aspect
@Component
class CoolBoy {

	//环绕通知
	@Around("com.zmk666.aop.ShuaiGe.haha()")
	public Object around(ProceedingJoinPoint pjp) {
		System.out.println("前置通知");
		Object[] args = pjp.getArgs();
		System.out.println(Arrays.toString(args));
		try {
			Object r = pjp.proceed(args);
			System.out.println("返回值通知");
			return r;
		} catch (Throwable e) {
			System.out.println("异常通知");
			e.printStackTrace();
		}
		System.out.println("后置通知");
		return 18.95;
	}
}
