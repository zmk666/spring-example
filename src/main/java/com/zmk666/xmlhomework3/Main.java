package com.zmk666.xmlhomework3;

import java.io.IOException;
import java.io.OutputStream;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource(locations= {"classpath:outputstream.cfg.xml"})
public class Main {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		ApplicationContext ac = new AnnotationConfigApplicationContext(Main.class);
		OutputStream out = ac.getBean(OutputStream.class);
		String str = "today is goodday the cloud is not found the air is soo cold my foot quik missing";
		out.write(str.getBytes());
	}

}
