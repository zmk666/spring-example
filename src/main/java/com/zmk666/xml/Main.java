package com.zmk666.xml;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;



@Configuration
@ImportResource(locations= {"classpath:app.cfg.xml"}) //引入xml配置文件
@Import(MyConfig.class) //引入MyConfig配置类
public class Main {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		ApplicationContext context =
				new AnnotationConfigApplicationContext(Main.class);
				//new FileSystemXmlApplicationContext("classpath:app.cfg.xml");//此类不支持注解
		
		Date date = (Date) context.getBean("hongkong_back_day");
		
		SimpleDateFormat sdf = context.getBean(SimpleDateFormat.class);
		System.out.println(sdf.format(date));
		
		Person person = context.getBean(Person.class);
		System.out.println(person);
		
		System.out.println(context.getBean(Cat.class));
		
		Calendar calendar = context.getBean(Calendar.class);
		System.out.println(calendar.getClass());
		
		System.out.println(context.getBean(People.class));
		
	}

}
