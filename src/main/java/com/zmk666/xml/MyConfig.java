package com.zmk666.xml;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfig {

	@Bean
	public Integer age() {
		return (int) (Math.random()*23+16);
	}
	
}
