package com.zmk666.xml;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

public class People {
	private String name;
	
	private Date birthday;
	
	@Autowired
	private Integer age;
	
	@Autowired
	private Double xixi;

	public People(String name, Date birthday) {
		super();
		this.name = name;
		this.birthday = birthday;
	}
	
	

	public Double getXixi() {
		return xixi;
	}



	public void setXixi(Double xixi) {
		this.xixi = xixi;
	}



	public People() {
		super();
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public Date getBirthday() {
		return birthday;
	}



	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}



	public Integer getAge() {
		return age;
	}



	public void setAge(Integer age) {
		this.age = age;
	}



	@Override
	public String toString() {
		return "People [name=" + name + ", birthday=" + birthday + ", age=" + age + ", xixi=" + xixi + "]";
	}








	
	
}	
