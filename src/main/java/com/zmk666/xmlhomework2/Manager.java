package com.zmk666.xmlhomework2;

import org.springframework.beans.factory.annotation.Autowired;

public class Manager {
	
	private String name;
	
	private Integer age;
	
	private Character sex;
	
	@Autowired
	private Secretary secretary;
	
	@Autowired
	private Driver driver;

	public Manager() {
		super();
	}

	public Manager(String name, Integer age, Character sex, Secretary secretary, Driver driver) {
		super();
		this.name = name;
		this.age = age;
		this.sex = sex;
		this.secretary = secretary;
		this.driver = driver;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Character getSex() {
		return sex;
	}

	public void setSex(Character sex) {
		this.sex = sex;
	}

	public Secretary getSecretary() {
		return secretary;
	}

	public void setSecretary(Secretary secretary) {
		this.secretary = secretary;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	@Override
	public String toString() {
		return "Manager [name=" + name + ", age=" + age + ", sex=" + sex + ", secretary=" + secretary + ", driver="
				+ driver + "]";
	}
	
}

