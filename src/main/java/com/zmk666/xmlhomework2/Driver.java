package com.zmk666.xmlhomework2;

import org.springframework.beans.factory.annotation.Autowired;

public class Driver {
	
	private String name;
	
	private Integer age;
	
	private Character sex;
	
	@Autowired
	private Secretary wife;

	public Driver() {
		super();
	}

	public Driver(String name, Integer age, Character sex, Secretary wife) {
		super();
		this.name = name;
		this.age = age;
		this.sex = sex;
		this.wife = wife;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Character getSex() {
		return sex;
	}

	public void setSex(Character sex) {
		this.sex = sex;
	}

	public Secretary getWife() {
		return wife;
	}

	public void setWife(Secretary wife) {
		this.wife = wife;
	}

	@Override
	public String toString() {
		return "Driver [name=" + name + ", age=" + age + ", sex=" + sex + ", wife=" + wife + "]";
	}
	
}
