package com.zmk666.xmlhomework2;

import org.springframework.beans.factory.annotation.Autowired;

public class Secretary {
	
	private String name;
	
	private Integer age;
	
	private Character sex;
	
	@Autowired
	private Driver husband;

	public Secretary() {
		super();
	}

	public Secretary(String name, Integer age, Character sex, Driver husband) {
		super();
		this.name = name;
		this.age = age;
		this.sex = sex;
		this.husband = husband;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Character getSex() {
		return sex;
	}

	public void setSex(Character sex) {
		this.sex = sex;
	}

	public Driver getHusband() {
		return husband;
	}

	public void setHusband(Driver husband) {
		this.husband = husband;
	}

	@Override
	public String toString() {
		return "Secretary [name=" + name + ", age=" + age + ", sex=" + sex + ", husband=" + "�Ϲ��Ұ���" + "]";
	}
	
}
