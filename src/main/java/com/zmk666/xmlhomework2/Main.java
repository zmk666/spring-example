package com.zmk666.xmlhomework2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource(locations= {"classpath:manager.cfg.xml"})
public class Main {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		ApplicationContext ac = new AnnotationConfigApplicationContext(Main.class);
		Manager manager = ac.getBean(Manager.class);
		System.out.println(manager);
	}

}
