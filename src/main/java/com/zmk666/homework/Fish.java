package com.zmk666.homework;

public class Fish {
	private Water water;
	private String type;
	private String address;
	
	public Fish(Water water, String type, String address) {
		super();
		this.water = water;
		this.type = type;
		this.address = address;
	}
	public Fish() {
		super();
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Water getWater() {
		return water;
	}
	public void setWater(Water water) {
		this.water = water;
	}
	@Override
	public String toString() {
		return "Fish [water=" + water + ", type=" + type + ", address=" + address + "]";
	}
	
}
