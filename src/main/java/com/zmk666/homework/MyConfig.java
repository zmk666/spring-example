package com.zmk666.homework;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class MyConfig {
	
	@Bean
	public Cat cat() {
		return new Cat("喵喵",2,"pink",fish());
	}
	
	@Bean
	public Fish fish() {
		return new Fish(water(),"黄金机械鲨王","长江");
		
	}
	
	@Bean
	public Water water() {
		return new Water("农夫山泉","广西桂林");
		
	}
	
}
