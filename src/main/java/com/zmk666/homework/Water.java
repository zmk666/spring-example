package com.zmk666.homework;



public class Water {
	private String brand;
	private String origin;
	public Water() {
		super();
	}
	
	public Water(String brand, String origin) {
		super();
		this.brand = brand;
		this.origin = origin;
	}

	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	@Override
	public String toString() {
		return "Water [brand=" + brand + ", origin=" + origin + "]";
	}
	
}
