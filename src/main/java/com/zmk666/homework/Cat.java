package com.zmk666.homework;


public class Cat {
	private Fish fish;
	private String name;
	private Integer age;
	private String color;
	public Cat(String name, Integer age, String color,Fish fish) {
		super();
		this.name = name;
		this.age = age;
		this.color = color;
		this.fish = fish;
	}
	public Cat() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public Fish getFish() {
		return fish;
	}
	public void setFish(Fish fish) {
		this.fish = fish;
	}
	@Override
	public String toString() {
		return "Cat [fish=" + fish + ", name=" + name + ", age=" + age + ", color=" + color + "]";
	}
	
	
}
