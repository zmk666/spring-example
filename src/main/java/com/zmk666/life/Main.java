package com.zmk666.life;

import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Main {
	
	@Bean
	public Date date() {
		return new Date();
	}
	
	@Bean
	public BeanLife beanLife() {
		return new BeanLife();
	}

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		ApplicationContext ac = new AnnotationConfigApplicationContext(Main.class);
	}

}
