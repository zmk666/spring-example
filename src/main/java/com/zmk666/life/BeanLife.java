package com.zmk666.life;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class BeanLife implements BeanNameAware,BeanFactoryAware,InitializingBean,
									DisposableBean,BeanPostProcessor,ApplicationContextAware{

	public BeanLife() {
		System.out.println("bean被创建了");
	}

	@Override
	public void setBeanName(String name) {
		System.out.println(name);
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		System.out.println("BeanFactory");
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("bean被初始化的时候做点什么好，相当于init方法");
	}

	@Override
	public void destroy() throws Exception {
		System.out.println("bean被销毁了");
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		System.out.println(beanName);
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("after------------"+beanName);
		return bean;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		
	}

	
}
