package com.zmk666.aoppoint;




import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan
public class Main {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = 
				new AnnotationConfigApplicationContext(Main.class);
		
		Person person = context.getBean(Person.class);
		
		System.out.println(person.getMoney());
		
		person.singsong("����һ����");
		
		person.xixi("�Ǻ�");
		
		person.sing(new Cat());
		
		Object date = context.getBean("myDate");
		
		System.out.println(date);
		
		context.close();
	}
	

}
