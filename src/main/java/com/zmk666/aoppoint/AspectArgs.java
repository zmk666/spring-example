package com.zmk666.aoppoint;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectArgs {

	@Around("args(aa)")      
	public Object around(ProceedingJoinPoint pjp,String aa) {
		System.out.println("---------------"+aa);
		Object[] args = pjp.getArgs();
		try {
			return pjp.proceed(args);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Around("@args(com.zmk666.aoppoint.Changge)")
	public Object around(ProceedingJoinPoint pjp) {
		System.out.println("---------------è��");
		Object[] args = pjp.getArgs();
		try {
			return pjp.proceed(args);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Around("target(com.zmk666.aoppoint.MyDate)")
	public Object around1(ProceedingJoinPoint pjp) {
		System.out.println("**********************");
		Object[] args = pjp.getArgs();
		try {
			return pjp.proceed(args);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
