package com.zmk666.aoppoint;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class MyDate extends Date{
	
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "今天天气真好";
	}
}
