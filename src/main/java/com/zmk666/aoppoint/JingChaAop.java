package com.zmk666.aoppoint;

import java.io.BufferedReader;
import java.io.FileReader;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class JingChaAop {

	@Pointcut("execution(* com.zmk666.aoppoint.Person.*(String))")  //与或非
	public void zq() {}
	
	@Pointcut("@annotation(com.zmk666.aoppoint.DaGu)")  //所有标注了这个注解的方法都要拦截
	public void dg() {}
	
	
	@Pointcut("@annotation(com.zmk666.aoppoint.FromFile)")
	public void FromFile() {}
	
	@Around("@annotation(ff)")  
	public Object round(ProceedingJoinPoint pjp,FromFile ff) {
		System.out.println(ff.value());
		
//		MethodSignature signature =(MethodSignature) pjp.getSignature();
//		Method method = signature.getMethod();
//		com.zmk666.aoppoint.FromFile annotation = method.getAnnotation(FromFile.class);
//		String value = annotation.value();
//		System.out.println(value);
		
		
		//java1.7之后可以直接这样写，会帮你自动关流
		try(FileReader filereader = new FileReader(ff.value());
				BufferedReader bufferedReader = new BufferedReader(filereader);
				) {
			String str = "";
			StringBuilder sb = new StringBuilder();
			while((str=bufferedReader.readLine()) != null) {
				sb.append(str);
			}
			return pjp.proceed(new Object[] {sb.toString()});
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
