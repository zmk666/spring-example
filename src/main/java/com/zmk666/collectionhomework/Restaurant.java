package com.zmk666.collectionhomework;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Restaurant {
	
	private List<Food> menu;
	
	private Map<String,Table> tables;
	
	private Set<Waiter> waitress;

	public List<Food> getMenu() {
		return menu;
	}

	public void setMenu(List<Food> menu) {
		this.menu = menu;
	}


	public Map<String, Table> getTables() {
		return tables;
	}

	public void setTables(Map<String, Table> tables) {
		this.tables = tables;
	}

	public Set<Waiter> getWaitress() {
		return waitress;
	}

	public void setWaitress(Set<Waiter> waitress) {
		this.waitress = waitress;
	}

	@Override
	public String toString() {
		return "Restaurant [menu=" + menu + ", tables=" + tables + ", waitress=" + waitress + "]";
	}
	
}
