package com.zmk666.collectionhomework;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource(locations="classpath:util/restaurant.cfg.xml")
public class Main {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		ApplicationContext ac = new AnnotationConfigApplicationContext(Main.class);
		
		Restaurant bean = ac.getBean(Restaurant.class);
		System.out.println(bean);
	}

}
