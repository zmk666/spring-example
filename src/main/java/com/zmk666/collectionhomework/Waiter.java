package com.zmk666.collectionhomework;

import org.springframework.beans.factory.annotation.Value;

public class Waiter {

	private String name;
	
	private Integer age;
	
	private Character sex;
	
	@Value("#{table.size()*1000}")
	private Integer salary;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Character getSex() {
		return sex;
	}

	public void setSex(Character sex) {
		this.sex = sex;
	}

	@Override
	public String toString() {
		return "Waiter [name=" + name + ", age=" + age + ", sex=" + sex + ", salary=" + salary + "]";
	}
	
}
