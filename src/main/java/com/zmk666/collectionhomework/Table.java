package com.zmk666.collectionhomework;

public class Table {
	
	private Integer number;
	
	private Integer capacity;

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	@Override
	public String toString() {
		return "Table [number=" + number + ", capacity=" + capacity + "]";
	}
	
}
