package com.zmk666.aophomework;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Check {

	@Around("execution(* com.zmk666.aophomework.Museum.visit(..))")
	public Object around(ProceedingJoinPoint pjp) {
		System.out.println("保安搜身，检查有没有携带枪支");
		Object[] args = pjp.getArgs();
		Object r = 0;
		try {
			r = pjp.proceed(args);
		} catch (Throwable e) {
			System.out.println("躲在厕所躲一天");
		}
		System.out.println("妈妈要求写一篇5000字的感想");
		return r;
	}
	
}
