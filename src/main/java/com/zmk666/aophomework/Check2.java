package com.zmk666.aophomework;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class Check2 {
		
	@Around("execution(* com.zmk666.aophomework.Museum.visit(..))")
	public Object around(ProceedingJoinPoint pjp) {
		System.out.println("工作人员检查有没有买票");
		Object[] args = pjp.getArgs();
		try {
			pjp.proceed(args);
		} catch (Throwable e) {
			System.out.println("躲在厕所里躲一天");
		}
		System.out.println("妈妈要求写一篇5000字的感想");
		return null;
	}
	
}
