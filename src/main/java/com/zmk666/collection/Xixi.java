package com.zmk666.collection;

import java.io.Serializable;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Primary
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE) // 容器中每次拿同一个对象都是相同的，此注解可以让拿到的对象的地址不同
public class Xixi implements Serializable {

	private static final long serialVersionUID = 1L;

}
