package com.zmk666.collection;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

public class Man implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Value("#{dsz.name}")
	private String name;
	
	//@Value("#{${man.age}+ 10}")
	@Value("#{dsz.name.length()}")
	private Integer age;
	
	@Value("#{T(System).getProperty('os.version') matches '.+'}")
	private Boolean hasEmail;
	
//	@Autowired
//	@Qualifier("dsz")
	@Value("#{dsz}")
	private Map<String,Object> map;
	
	private List<String> lists;

	public List<String> getLists() {
		return lists;
	}

	public void setLists(List<String> lists) {
		this.lists = lists;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Map<String, Object> getMap() {
		return map;
	}

	public void setMap(Map<String, Object> map) {
		this.map = map;
	}

	@Override
	public String toString() {
		return "Man [name=" + name + ", age=" + age + ", hasEmail=" + hasEmail + ", map=" + map + ", lists=" + lists
				+ "]";
	}

	
}
