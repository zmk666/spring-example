package com.zmk666.collection;

import java.io.Serializable;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ImportResource(locations = "classpath:util/app.cfg.xml")
@PropertySource("classpath:util/db.properties")
@ComponentScan
public class CollectionDemo {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		ApplicationContext ac = new AnnotationConfigApplicationContext(CollectionDemo.class);
		Man bean = ac.getBean(Man.class);
		System.out.println(bean);

		@SuppressWarnings("unchecked")
		Map<String, String> bean2 = (Map<String, String>) ac.getBean("dsz");
		System.out.println(bean2);

		Object bean3 = ac.getBean("dataBase");
		System.out.println(bean3);

		// 容器中的内容是唯一的
		Serializable bean4 = ac.getBean(Serializable.class);
		Serializable bean5 = ac.getBean(Serializable.class);
		System.out.println(bean4 == bean5);
	}

}
