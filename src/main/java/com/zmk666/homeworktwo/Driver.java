package com.zmk666.homeworktwo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Driver {
	@Autowired
	private Car car;
	@Autowired
	private Wife wife;
	private String name;
	private String bithplace;
	private Long phone;
	public Driver() {
		super();
	}
	public Driver(Car car, Wife wife, String name, String bithplace, Long phone) {
		super();
		this.car = car;
		this.wife = wife;
		this.name = name;
		this.bithplace = bithplace;
		this.phone = phone;
	}
	public Car getCar() {
		return car;
	}
	public void setCar(Car car) {
		this.car = car;
	}
	public Wife getWife() {
		return wife;
	}
	public void setWife(Wife wife) {
		this.wife = wife;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBithplace() {
		return bithplace;
	}
	public void setBithplace(String bithplace) {
		this.bithplace = bithplace;
	}
	public Long getPhone() {
		return phone;
	}
	public void setPhone(Long phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		return "Driver [car=" + car + ", wife=" + wife + ", name=" + name + ", bithplace=" + bithplace + ", phone="
				+ phone + "]";
	}
	
}
