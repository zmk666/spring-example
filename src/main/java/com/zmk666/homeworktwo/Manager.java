package com.zmk666.homeworktwo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Manager {
	@Autowired   //默认是通过类型获取
	@Qualifier("wife")  //加一个@Qualifier表示让它通过名称获取
	private Wife wife;
	private String name;
	private Integer age;
	private Character sex;
	public Manager() {
		super();
	}
	public Manager(Wife wife, String name, Integer age, Character sex) {
		super();
		this.wife = wife;
		this.name = name;
		this.age = age;
		this.sex = sex;
	}
	public Wife getwife() {
		return wife;
	}
	public void setwife(Wife wife) {
		this.wife = wife;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Character getSex() {
		return sex;
	}
	public void setSex(Character sex) {
		this.sex = sex;
	}
	@Override
	public String toString() {
		return "Manager [wife=" + wife + ", name=" + name + ", age=" + age + ", sex=" + sex + "]";
	}
	
}
