package com.zmk666.homeworktwo;

import org.springframework.stereotype.Component;

@Component
public class Car {
	private String type;
	private String origin;
	public Car() {
		
	}
	public Car(String type, String origin) {
		this.type = type;
		this.origin = origin;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	@Override
	public String toString() {
		return "Car [type=" + type + ", origin=" + origin + "]";
	}
	
}