package com.zmk666.homeworktwo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Company {
	@Autowired
	private Manager manager;
	@Autowired
	private Driver driver;
	public Company() {
		
	}
	public Company(Manager manager, Driver driver) {
		this.manager = manager;
		this.driver = driver;
	}
	public Manager getManager() {
		return manager;
	}
	public void setManager(Manager manager) {
		this.manager = manager;
	}
	public Driver getDriver() {
		return driver;
	}
	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	@Override
	public String toString() {
		return "Company [manager=" + manager + ", driver=" + driver + "]";
	}
	
}
