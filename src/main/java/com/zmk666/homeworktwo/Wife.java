package com.zmk666.homeworktwo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Wife {
	private String name;
	private String beauty;
	@Autowired
	private Driver driver;
	public Wife() {
		super();
	}
	public Wife(String name, String beauty, Driver driver) {
		super();
		this.name = name;
		this.beauty = beauty;
		this.driver = driver;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBeauty() {
		return beauty;
	}
	public void setBeauty(String beauty) {
		this.beauty = beauty;
	}
	public Driver getDriver() {
		return driver;
	}
	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	@Override
	public String toString() {
		return "Wife [name=" + name + ", beauty=" + beauty + ", driver=�ɷ�" + "]";
	}
	
}
