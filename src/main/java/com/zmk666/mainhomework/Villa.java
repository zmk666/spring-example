package com.zmk666.mainhomework;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Villa {
	private String name;
	@Autowired
	private Servant servant;
	public Villa() {
		super();
		this.name = "��������";
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Servant getServant() {
		return servant;
	}

	public void setServant(Servant servant) {
		this.servant = servant;
	}

	@Override
	public String toString() {
		return "Villa [name=" + name + ", servant=" + servant + "]";
	}
	
}
