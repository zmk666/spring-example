package com.zmk666.mainhomework;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Xiaoming {
	private String name;
	private Integer age;
	private Character sex;
	@Autowired
	private GirlFriend girlfriend;
	public Xiaoming() {
		super();
		this.name = "С��";
		this.age = 20;
		this.sex = '��';
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Character getSex() {
		return sex;
	}
	public void setSex(Character sex) {
		this.sex = sex;
	}
	public GirlFriend getGirlfriend() {
		return girlfriend;
	}
	public void setGirlfriend(GirlFriend girlfriend) {
		this.girlfriend = girlfriend;
	}
	@Override
	public String toString() {
		return "Xiaoming [name=" + name + ", age=" + age + ", sex=" + sex + ", girlfriend=" + girlfriend + "]";
	}

	
}
