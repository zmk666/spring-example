package com.zmk666.mainhomework;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GirlFriend {
	private String name;
	private Integer age;
	private Character sex;
	@Autowired
	private Villa villa;
	public GirlFriend() {
		super();
		this.name = "С��";
		this.age = 19;
		this.sex = 'Ů';
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Character getSex() {
		return sex;
	}
	public void setSex(Character sex) {
		this.sex = sex;
	}
	public Villa getVilla() {
		return villa;
	}
	public void setVilla(Villa villa) {
		this.villa = villa;
	}
	@Override
	public String toString() {
		return "GirlFriend [name=" + name + ", age=" + age + ", sex=" + sex + ", villa=" + villa + "]";
	}
	
}
