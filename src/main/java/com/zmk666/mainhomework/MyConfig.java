package com.zmk666.mainhomework;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration   //这两个包的路径写不写都无所谓，默认扫描本包，只是写来练手
@ComponentScan(basePackageClasses= {MyConfig.class},basePackages= {"com.zmk666.mainhomework"})
public class MyConfig {

}
