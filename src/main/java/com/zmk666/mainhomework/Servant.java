package com.zmk666.mainhomework;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Servant {
	private String name;
	@Autowired
	private Xiaoming xiaoming;
	public Servant() {
		super();
		this.name = "魔法女巫";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Xiaoming getXiaoming() {
		return xiaoming;
	}
	public void setXiaoming(Xiaoming xiaoming) {
		this.xiaoming = xiaoming;
	}
	@Override
	public String toString() {
		return "Servant [name=" + name + ", xiaoming=" + "小明明" + "]";
	}
	
}	
