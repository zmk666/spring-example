package com.zmk666.haha;

import java.util.Calendar;
import java.util.Date;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses= {MyConfig.class},basePackages= {"com.zmk666.homeworktwo"})
public class MyConfig {

	@Bean
	public Date date() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR,1949);
		c.set(Calendar.MONTH, 9);
		c.set(Calendar.DATE, 1);
		return c.getTime();
	}
	
}
