package com.zmk666.xmlhomework;


import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource(locations= {"classpath:person.cfg.xml"})
public class Main {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		ApplicationContext ac = new AnnotationConfigApplicationContext(Main.class);
		
		Person xiaoming =(Person) ac.getBean("xiaoming");
		System.out.println(xiaoming);
		
		Person xiaofang = (Person) ac.getBean("xiaofang");
		System.out.println(xiaofang);
		
		Calendar c = Calendar.getInstance();
		c.set(1997, 2, 28);
		c.add(Calendar.DATE, 128);
		System.out.println(c.getTime());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(sdf.format(c.getTime()));
	}

}
