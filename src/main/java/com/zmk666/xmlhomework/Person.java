package com.zmk666.xmlhomework;

import java.util.Date;

public class Person {
	
	private String name;
	
	private Integer age;
	
	private Character sex;
	
	private Date birthday;

	public Person() {
		super();
	}

	public Person(String name, Integer age, Character sex, Date birthday) {
		super();
		this.name = name;
		this.age = age;
		this.sex = sex;
		this.birthday = birthday;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Character getSex() {
		return sex;
	}

	public void setSex(Character sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", sex=" + sex + ", birthday=" + birthday + "]";
	}
	
	
}
